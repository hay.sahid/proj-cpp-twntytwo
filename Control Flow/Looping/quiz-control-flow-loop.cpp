#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	
	string nama, nim, ket;
	int nilai_uts = 0, nilai_uas = 0;
	float rerata;
	
	for(int i=1; i<=2; i++)
	{
		cout<<"Perulangan ke " << i <<endl;
		cout<<"---------------------------------------------------"<<endl;
		cout<<"Program Menentukan Lulus Tidaknya Mahasiswa "<<endl;
		cout<<"---------------------------------------------------"<<endl;
		
		cout<<"Nama Mahasiswa   : ";
		// menghindari skipped input
		cin.ignore();
		getline(cin, nama);
		
		cout<<"\nNIM \t\t : "; getline(cin,nim);
		cout<<"\nNilai UTS  \t : "; cin>>nilai_uts;
		cout<<"\nNilai UAS  \t : "; cin>>nilai_uas;
		
			
		
		rerata = (nilai_uts + nilai_uas) / 2;
		
		cout<<"\n\nNilai Rata - Rata : "<<rerata <<endl;
		
		if(rerata >= 70){
			cout<<"\nKeterangan (L=LULUS / T=TIDAK) = L " <<endl;
		}
		else {
			cout<<"\nKeterangan (L=LULUS / T=TIDAK) = T " <<endl;
		}
		
		cout<<endl <<endl;	
	}
	
	getch();
	
}
