#include <iostream>

/* start - Declaring a Final Value */
#define null 0
#define besar_pajak 0.25
#define satuan_tunj 100000
#define gaji_kotor 750000
/* end - Declaring a Final Value */

using namespace std;

int main() {
	
	/* start - Variable Declaration and it's Default Value */
	float gaji_bersih, pajak, tunjangan_anak = null;
	int status, jumlah_anak = null;
	string nama,jabatan,choose = "N"; 
	/* end - Variable Declaration and it's Default Value */
	
	/* start - User Input */
	input:	
		cout << "Masukan Nama : "; cin >> nama;
		cout << "Masukan Jabatan : "; cin >> jabatan;
		cout << endl;
		
		/* start - Show The Menu */	
		status:
			cout << "1. Menikah\n";
			cout << "2. Lajang\n";
		/* end - Show The Menu */	
			
		cout << "Pilih Status : ";	
		cin >> status;      
		
		if(status < 2) {
			cout << "Jumlah Anak : "; cin >> jumlah_anak;	
		}
	/* end - User Input */
	
	/* start - Clear View */
	system("cls");
	/* end - Clear View */
	
	/* start - Business Logic */
	/* expl : We jump right into jumlah_anak condition instead of status condition, 
		because if jumlah_anak is greater than 0, it means karyawan status is menikah  */
	if(jumlah_anak > null) {
		tunjangan_anak = jumlah_anak * satuan_tunj;
	}
		
	pajak = gaji_kotor * besar_pajak;
	gaji_bersih = (gaji_kotor + tunjangan_anak) -pajak;	
	/* end - Business Logic */
	
	/* start - Output View */
	cout << " | Rincian Gaji Sdr " << nama << " |" << endl << endl;
	cout << " | Gaji Bersih\t\t" << " : Rp." << gaji_bersih << "\t |" << endl;
	cout << " | Tunjangan Anak\t" << " : Rp." << tunjangan_anak << "\t |" << endl;
	cout << " | Pajak\t\t" << " : Rp." << pajak << "\t |" << endl << endl;
	cout << " \nApakah Anda Ingin Kembali? " << " [Y/N]" << endl; cin >> choose;
	/* end - Output View */

	/* start - Back To User Input */
	if(choose != "N") {
		system("cls");
		goto input;
	}
	/* end - Back To User Input */
	
	return null;
}
